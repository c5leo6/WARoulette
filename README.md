##Esta es la aplicación WARoulette. Permite gestionar algunas apuestas a través de APIs
##Para usarla, por favor tenga en cuenta las APIs que puede usar, que son las siguientes:

##  **  Crear ruleta: la API retorna el ID de la ruleta. Por defecto, la ruleta estará cerrada. /api/CreateRoulette
##  **  Abrir ruleta: /api/OpenRoulette/{RouletteId} donde {RouletteId} es un Id de ruleta existente. Se retornará un BadRequest si el Id de la ruleta no existe.
##  **  Hacer apuesta: /api/CreateBet/{idRoulette}/{value}/{amount} donde {idRoulette} es un Id de ruleta existente, {value} es el valor de la apuesta (número del 1 al 36 o color rojo o negro) y {amount} es el monto apostado. Se retornará un BadRequest si el Id de la ruleta no existe o si la ruleta no está abierta para apuestas.
##  **  Cerrar ruleta: /api/CloseRoulette/{idRoulette} donde {idRoulette} es un Id de ruleta existente. La API retornará las apuestas realizadas a dicha ruleta. Se retornará un BadRequest si el Id de la ruleta no existe.
##  **  Ver ruletas creadas: la API retornará las ruletas creadas con su estado (0 si está cerrada o 1 si está abierta).  /api/GetRoulettes

##Gracias por utilizar esta app. :)

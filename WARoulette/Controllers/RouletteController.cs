﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WARoulette.Models;

namespace WARoulette.Controllers
{
    public class RouletteController : ApiController
    {
        [Route("api/CreateRoulette")]
        [HttpPost]
        public IHttpActionResult CreateRoulette()
        {
            IHttpActionResult Result;
            try
            {
                var response = SQLConnection.CreateRoulette();
                if (response.Success)
                {
                    Result = Ok("Ruleta creada. El ID de la ruleta es " + response.ReturnedNumber + ".");
                }
                else
                {
                    Result = BadRequest("Ha ocurrido un error al crear la ruleta. Por favor revise la conexión a la base de datos.");
                }
            }
            catch (Exception ex)
            {
                Result = BadRequest("Ha ocurrido un error al realizar el proceso. Detalles: " + ex.Message);
            }
            return Result;
        }
        [Route("api/OpenRoulette/{RouletteId}")]
        [HttpPost]
        public IHttpActionResult OpenRoulette(int RouletteId)
        {
            IHttpActionResult Result;
            try
            {
                var response = SQLConnection.OpenRoulette(RouletteId);
                if (response.Success)
                {
                    if (response.ReturnedNumber == RouletteId)
                    {
                        Result = Ok("La ruleta con ID " + RouletteId + " ahora está recibiendo apuestas. ¡A ganar!");
                    }
                    else
                    {
                        Result = BadRequest(response.Message);
                    }
                }
                else
                {
                    Result = BadRequest("Ha ocurrido un error al crear la ruleta. Por favor revise la conexión a la base de datos.");
                }
            }
            catch(Exception ex)
            {
                Result = BadRequest("Ha ocurrido un error al realizar el proceso. Detalles: " + ex.Message);
            }
            return Result;
        }

        [Route("api/CreateBet/{idRoulette}/{value}/{amount}")]
        [HttpPost]
        public IHttpActionResult CreateBet(int idRoulette, string value, int amount)
        {
            IHttpActionResult Result;
            try
            {
                var re = Request;
                var headers = re.Headers;
                if (headers.Contains("UserId"))
                {
                    string userId = headers.GetValues("UserId").First();
                    Bet bet = new Bet()
                    {
                        RouletteId = idRoulette,
                        Value = value,
                        Amount = amount,
                        UserId = int.Parse(userId)
                    };
                    var response = SQLConnection.CreateBet(bet);
                    if (response.Success)
                    {
                        if (response.ReturnedNumber == bet.RouletteId)
                        {
                            Result = Ok("Apuesta confirmada para la ruleta con ID " + bet.RouletteId + ". Mucha suerte. :)");
                        }
                        else
                        {
                            Result = BadRequest(response.Message);
                        }
                    }
                    else
                    {
                        Result = BadRequest("Ha ocurrido un error al crear la ruleta. Por favor revise la conexión a la base de datos.");
                    }
                }
                else
                {
                    Result = BadRequest("El usuario no puede estar vacío. Recuerde usar el encabezado (header) 'UserId'.");
                }
            }
            catch (Exception ex)
            {
                Result = BadRequest("Ha ocurrido un error al realizar el proceso. Detalles: " + ex.Message);
            }
            return Result;
        }

        [Route("api/CloseRoulette/{idRoulette}")]
        [HttpPost]
        public IHttpActionResult CloseRoulette(int idRoulette)
        {
            IHttpActionResult Result;
            try
            {
                var re = Request;
                var headers = re.Headers;
                var response = SQLConnection.CloseRoulette(idRoulette);
                if (response.Success)
                {
                    if (response.ReturnedNumber == idRoulette)
                    {
                        Result = Ok(response.Results);
                    }
                    else
                    {
                        Result = BadRequest(response.Message);
                    }
                }
                else
                {
                    Result = BadRequest("Ha ocurrido un error al crear la ruleta. Por favor revise la conexión a la base de datos.");
                }
            }
            catch (Exception ex)
            {
                Result = BadRequest("Ha ocurrido un error al realizar el proceso. Detalles: " + ex.Message);
            }
            return Result;
        }

        [Route("api/GetRoulettes")]
        [HttpPost]
        public IHttpActionResult GetRoulettes()
        {
            IHttpActionResult Result;
            try
            {
                var re = Request;
                var headers = re.Headers;
                var response = SQLConnection.GetRoulettes();
                if (response.Success)
                {
                    Result = Ok(response.RouletteResults);
                }
                else
                {
                    Result = BadRequest("Ha ocurrido un error al crear la ruleta. Por favor revise la conexión a la base de datos.");
                }
            }
            catch (Exception ex)
            {
                Result = BadRequest("Ha ocurrido un error al realizar el proceso. Detalles: " + ex.Message);
            }
            return Result;
        }
    }
}

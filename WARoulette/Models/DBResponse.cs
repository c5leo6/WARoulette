﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WARoulette.Models
{
    public class DBResponse
    {
        public bool Success { get; set; }
        public int ReturnedNumber { get; set; }
        public string ReturnedChar { get; set; }
        public string Message { get; set; }
        public List<Bet> Results { get; set; }
        public List<Roulette> RouletteResults { get; set; }
    }
}
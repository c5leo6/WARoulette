﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Web;

namespace WARoulette.Models
{
    public class SQLConnection
    {
        public static SQLiteConnection connection;
        public static SQLiteConnection GetConnection()
        {
            var directory = AppDomain.CurrentDomain.BaseDirectory;
            SQLiteConnection objConnection = new SQLiteConnection("Data Source=" + directory + "RouletteGameDB.db;");
            return objConnection;
        }
        public static DBResponse CreateRoulette()
        {
            DBResponse response = new DBResponse();
            connection = GetConnection();
            if (connection.State == ConnectionState.Open)
                connection.Close();
            connection.Open();
            SQLiteCommand command = connection.CreateCommand();
            command.CommandText = "insert into Roulette (Opened) values(?)";
            command.Parameters.AddWithValue("Name", 0);
            command.ExecuteNonQuery();
            command = connection.CreateCommand();
            command.CommandText = "select max(Id) from Roulette";
            DataTable dt = new DataTable();
            SQLiteDataAdapter da = new SQLiteDataAdapter(command);
            da.Fill(dt);
            int Counter = int.Parse(dt.Rows[0].ItemArray[0].ToString());
            if (Counter > 0)
            {
                response.Success = true;
                response.ReturnedNumber = int.Parse(dt.Rows[0].ItemArray[0].ToString());
            }
            connection.Close();

            return response;
        }

        public static DBResponse OpenRoulette(int RouletteId)
        {
            DBResponse response = new DBResponse();
            connection = GetConnection();
            if (connection.State == ConnectionState.Open)
                connection.Close();
            connection.Open();
            if (RouletteExist(RouletteId))
            {
                SQLiteCommand command = connection.CreateCommand();
                command.CommandText = "update Roulette set Opened=1 where Id=?";
                command.Parameters.AddWithValue("Id", RouletteId);
                command.ExecuteNonQuery();
                response.ReturnedNumber = RouletteId;
            }
            else
            {
                response.ReturnedNumber = 0;
                response.Message = "La ruleta con ID " + RouletteId + " no existe. ";
            }
            response.Success = true;
            connection.Close();

            return response;
        }

        public static DBResponse CreateBet(Bet bet)
        {
            DBResponse response = new DBResponse();
            connection = GetConnection();
            if (connection.State == ConnectionState.Open)
                connection.Close();
            connection.Open();
            SQLiteCommand command = connection.CreateCommand();
            command.CommandText = "select count(*) from Roulette where Id=? and Opened=1";
            command.Parameters.AddWithValue("Id", bet.RouletteId);
            DataTable dt = new DataTable();
            SQLiteDataAdapter da = new SQLiteDataAdapter(command);
            da.Fill(dt);
            int Counter = int.Parse(dt.Rows[0].ItemArray[0].ToString());
            if (RouletteExist(bet.RouletteId))
            {
                command.CommandText = "insert into Wager (Value, Amount, IdRoulette, UserId) values('" + bet.Value + "', " + bet.Amount + ", " + bet.RouletteId + ", " + bet.UserId + ")";
                command.ExecuteNonQuery();
                response.ReturnedNumber = bet.RouletteId;
            }
            else
            {
                response.ReturnedNumber = 0;
                response.Message = "La ruleta con ID " + bet.RouletteId + " no existe o no está abierta para apuestas. ";
            }
            response.Success = true;
            connection.Close();

            return response;
        }

        public static bool RouletteExist(int IdRoulette)
        {
            bool Exist = false;
            SQLiteCommand command = connection.CreateCommand();
            command.CommandText = "select count(*) from Roulette where Id=? and Opened=1";
            command.Parameters.AddWithValue("Id", IdRoulette);
            DataTable dt = new DataTable();
            SQLiteDataAdapter da = new SQLiteDataAdapter(command);
            da.Fill(dt);
            Exist = int.Parse(dt.Rows[0].ItemArray[0].ToString()) > 0 ? true : false;
            return Exist;
        }

        public static DBResponse CloseRoulette(int IdRoulette)
        {
            DBResponse response = new DBResponse();
            SQLiteConnection connection = GetConnection();
            if (connection.State == ConnectionState.Open)
                connection.Close();
            connection.Open();
            if (RouletteExist(IdRoulette))
            {
                SQLiteCommand command = connection.CreateCommand();
                command.CommandText = "update Roulette set Opened=0 where Id=?";
                command.Parameters.AddWithValue("IdRoulette", IdRoulette);
                command.ExecuteNonQuery();
                command = connection.CreateCommand();
                command.CommandText = "select * from Wager where IdRoulette=?";
                command.Parameters.AddWithValue("IdRoulette", IdRoulette);
                DataTable dt = new DataTable();
                SQLiteDataAdapter da = new SQLiteDataAdapter(command);
                da.Fill(dt);
                List<Bet> BetList = new List<Bet>();
                foreach (DataRow row in dt.Rows)
                {
                    Bet bet = new Bet()
                    {
                        Amount = int.Parse(row["Amount"].ToString()),
                        RouletteId = int.Parse(row["IdRoulette"].ToString()),
                        UserId = int.Parse(row["UserId"].ToString()),
                        Value = row["Value"].ToString(),
                    };
                    BetList.Add(bet);
                }
                response.Results = BetList;
                response.ReturnedNumber = IdRoulette;
            }
            else
            {
                response.ReturnedNumber = 0;
                response.Message = "La ruleta con ID " + IdRoulette + " no existe. ";
            }
            response.Success = true;
            connection.Close();

            return response;
        }

        public static DBResponse GetRoulettes()
        {
            DBResponse response = new DBResponse();
            SQLiteConnection connection = GetConnection();
            if (connection.State == ConnectionState.Open)
                connection.Close();
            connection.Open();
            SQLiteCommand command = connection.CreateCommand();
            command.CommandText = "select Id, Opened as 'Abierto' from Roulette";
            DataTable dt = new DataTable();
            SQLiteDataAdapter da = new SQLiteDataAdapter(command);
            da.Fill(dt);
            List<Roulette> RouletteList = new List<Roulette>();
            foreach (DataRow row in dt.Rows)
            {
                Roulette bet = new Roulette()
                {
                    Id = int.Parse(row["Id"].ToString()),
                    Opened = row["Abierto"].ToString().Equals("1") ? true : false,
                };
                RouletteList.Add(bet);
            }
            response.RouletteResults = RouletteList;
            response.Success = true;
            connection.Close();

            return response;
        }
    }
}
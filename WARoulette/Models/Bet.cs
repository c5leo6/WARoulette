﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WARoulette.Models
{
    public class Bet
    {
        public int RouletteId { get; set; }
        public string Value { get; set; }
        public int Amount { get; set; }
        public int UserId { get; set; }
    }
}